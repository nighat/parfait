<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->date('date')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('web_address')->nullable();
            $table->string('reference')->nullable();
            $table->integer('reference_value')->nullable();
            $table->string('running')->nullable();
            $table->string('done')->nullable();
            $table->string('not_complete')->nullable();
            $table->string('timeline')->nullable();
            $table->string('progress')->nullable();
            $table->string('pp_date')->nullable();
            $table->integer('advance')->nullable();
            $table->integer('due')->nullable();
            $table->integer('total')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
           // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
