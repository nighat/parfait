<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin']],function(){

    Route::get('dashboard','DashboardController@index')->name('dashboard');
    Route::resource('groupclient','GroupclientController');
    Route::resource('clientservice','ClientserviceController');
    Route::resource('client','ClientController');
    Route::get('clientReport','ClientController@clientReport')->name('clientReport');
   Route::get('/clientInvoice/{id}', 'ClientController@clientInvoice');



    Route::resource('groupsupplier','GroupsupplierController');
    Route::resource('supplier','SupplierController');
    Route::get('supplierReport','SupplierController@supplierReport')->name('supplierReport');
    Route::resource('item','ItemController');
    Route::resource('product','ProductController');
    Route::get('productReport','ProductController@productReport')->name('productReport');
    Route::resource('purchase','PurchaseController');
    Route::get('purchaseReport','PurchaseController@purchaseReport')->name('purchaseReport');
    Route::resource('retire','RetireController');
    Route::resource('invoice','InvoiceController');

});


Route::group(['as'=>'author.','prefix'=>'author','namespace'=>'Author','middleware'=>['auth','author']],function(){
    Route::get('dashboard','DashboardController@index')->name('dashboard');

    Route::resource('groupclient','GroupclientController');
    Route::resource('clientservice','ClientserviceController');
    Route::resource('client','ClientController');
    Route::get('clientReport','ClientController@clientReport')->name('clientReport');
    Route::get('/clientInvoice/{id}', 'ClientController@clientInvoice');
});
