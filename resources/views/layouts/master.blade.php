<!DOCTYPE html>
<html>
<head>
    <title>Invoice Application</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('invoice/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('invoice/override.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('invoice/app.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    @yield('content')
</div>
</body>
@stack('scripts')

</html>