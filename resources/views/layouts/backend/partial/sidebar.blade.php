<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ asset('backend/images/user.png') }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>

                    <li> <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>Sign Out
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            @if(Request::is('admin*'))
            <li class="{{ Request::is('admin/dashboard')?'active':'' }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="material-icons col-cyan">dashboard</i>
                    <span>Dashboard</span>
                </a>
            </li>
               
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class=" material-icons col-blue">group</i>
                        <span>CMR</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>Client</span>
                            </a>
                            <ul class="ml-menu">
                                <li  class="{{ Request::is('admin/client*')?'active':'' }}">
                                    <a href="{{ route('admin.client.create') }}">Add Client</a>
                                </li>
                                <li  class="{{ Request::is('admin/client*')?'active':'' }}">
                                    <a href="{{ route('admin.client.index') }}"> Client List</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>Client Group</span>
                            </a>
                            <ul class="ml-menu">

                                <li class="{{ Request::is('admin/groupclient*')?'active':'' }}">
                                    <a href="{{ route('admin.groupclient.create') }}">Add Group</a>
                                </li>
                                <li class="{{ Request::is('admin/groupclient*')?'active':'' }}">
                                    <a href="{{ route('admin.groupclient.index') }}">Client Group List</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>Type of service</span>
                            </a>
                            <ul class="ml-menu">

                                <li class="{{ Request::is('admin/clientservice*')?'active':'' }}">
                                    <a href="{{ route('admin.clientservice.create') }}">Add Service</a>
                                </li>
                                <li class="{{ Request::is('admin/clientservice*')?'active':'' }}">
                                    <a href="{{ route('admin.clientservice.index') }}">Service List</a>
                                </li>

                            </ul>
                        </li>

                        {{--<li>--}}
                            {{--<a href="javascript:void(0);" class="menu-toggle">--}}
                                {{--<span>Supplier</span>--}}
                            {{--</a>--}}
                            {{--<ul class="ml-menu">--}}

                                {{--<li class="{{ Request::is('admin/supplier*')?'active':'' }}">--}}
                                    {{--<a href="{{ route('admin.supplier.create') }}">Add Supplier</a>--}}
                                {{--</li>--}}
                                {{--<li class="{{ Request::is('admin/supplier*')?'active':'' }}">--}}
                                    {{--<a href="{{ route('admin.supplier.index') }}">Supplier List</a>--}}
                                {{--</li>--}}

                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0);" class="menu-toggle">--}}
                                {{--<span>Supplier Group</span>--}}
                            {{--</a>--}}
                            {{--<ul class="ml-menu">--}}

                                {{--<li class="{{ Request::is('admin/groupsupplier*')?'active':'' }}">--}}
                                    {{--<a href="{{ route('admin.groupsupplier.create') }}">Add Group</a>--}}
                                {{--</li>--}}
                                {{--<li class="{{ Request::is('admin/groupsupplier*')?'active':'' }}">--}}
                                    {{--<a href="{{ route('admin.groupsupplier.index') }}">Supplier Group List</a>--}}
                                {{--</li>--}}

                            {{--</ul>--}}
                        {{--</li>--}}
                    </ul>

                </li>


                <li class="header">REPORTS</li>

                <li>
                    <a href="{{ route('admin.clientReport') }}">
                        <i class="material-icons col-amber">call_to_action</i>
                        <span>Client Report</span>
                    </a>
                </li>





            <li class="header"> System</li>
            <li class=""> <a href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="material-icons">input</i><span>Logout</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
            @endif


            @if(Request::is('author*'))
                <li class="{{ Request::is('author/dashboard')?'active':'' }}">
                    <a href="{{ route('author.dashboard') }}">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class=" material-icons col-blue">group</i>
                        <span>CMR</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>Client</span>
                            </a>
                            <ul class="ml-menu">
                                <li  class="{{ Request::is('author/client*')?'active':'' }}">
                                    <a href="{{ route('author.client.create') }}">Add Client</a>
                                </li>
                                <li  class="{{ Request::is('author/client*')?'active':'' }}">
                                    <a href="{{ route('author.client.index') }}"> Client List</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>Client Group</span>
                            </a>
                            <ul class="ml-menu">

                                <li class="{{ Request::is('author/groupclient*')?'active':'' }}">
                                    <a href="{{ route('author.groupclient.create') }}">Add Group</a>
                                </li>
                                <li class="{{ Request::is('author/groupclient*')?'active':'' }}">
                                    <a href="{{ route('author.groupclient.index') }}">Client Group List</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>Type of service</span>
                            </a>
                            <ul class="ml-menu">

                                <li class="{{ Request::is('author/clientservice*')?'active':'' }}">
                                    <a href="{{ route('author.clientservice.create') }}">Add Service</a>
                                </li>
                                <li class="{{ Request::is('author/clientservice*')?'active':'' }}">
                                    <a href="{{ route('author.clientservice.index') }}">Service List</a>
                                </li>

                            </ul>
                        </li>



                    </ul>

                </li>


                <li class="header">REPORTS</li>

                <li>
                    <a href="{{ route('author.clientReport') }}">
                        <i class="material-icons col-amber">call_to_action</i>
                        <span>Client Report</span>
                    </a>
                </li>
                <li class="header"> System</li>
                <li class=""> <a href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="material-icons">input</i><span>Logout</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            @endif

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2018 - 2019 <a href="javascript:void(0);">Nighat Parvin</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>