
@extends('layouts.backend.app')
@section('title','Client')

@push('css')
        <!-- JQuery DataTable Css -->
<link href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <a  class="btn bg-primary waves-effect " href="{{ route('admin.client.create') }}">
                <i class="material-icons">add</i>
                <span>Add New Client </span>
            </a>
            <a  class="btn bg-primary waves-effect " href="{{ route('admin.client.index') }}">
                <i class="material-icons">arrow_back</i>
                <span>Back List </span>
            </a>
        </div>
        <!-- #END# Basic Examples -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            All Client Group
                            <span class="badge bg-blue">{{ $clients->count() }}</span>
                        </h2>


                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Group </th>
                                    <th>Done</th>
                                    <th>Running</th>
                                    <th>Not Complete</th>
                                    <th>Status</th>
                                    <th>Advance</th>
                                    <th>Due</th>
                                    <th>Total</th>




                                </tr>
                                </thead>
                                <tfoot>
                                <tr>

                                    <th>Name</th>
                                    <th>Group </th>
                                    <th>Done</th>
                                    <th>Running</th>
                                    <th>Not Complete</th>
                                    <th>Status</th>
                                    <th>Advance</th>
                                    <th>Due</th>
                                    <th>Total</th>


                                </tr>
                                </tfoot>
                                <tbody>



                                @foreach($clients as $client)
                                    <tr>

                                        <td>{{ $client->name}}</td>
                                        <td>   @foreach($client->groupclients as $groupclient)

                                                {{ $groupclient->name }} <br/>
                                            @endforeach</td>
                                        <td>{{ $client->done }}</td>
                                        <td>{{ $client->running }}</td>
                                        <td>{{ $client->not_complete}}</td>
                                        <td>{{ $client->status}}</td>
                                        <td>{{ $client->advance}}</td>
                                        <td>{{ $client->total-$client->advance}}</td>
                                        <td>{{ $client->total}}</td>



                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- #END# Exportable Table -->
    </div>
    @endsection

    @push('js')
            <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('backend/js/pages/tables/jquery-datatable.js') }}"></script>

    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function deletegroupclient(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                event.preventDefault();
                document.getElementById('delete-form-'+id).submit();
            }else if (
                    // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
            ) {
                swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                )
            }
        })
        }
    </script>
    @endpush














