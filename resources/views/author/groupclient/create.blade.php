@extends('layouts.backend.app')

@section('title','Group Client')

@push('css')

@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ADD NEW GROUPCLIENT
                        </h2>
                    </div>
                    <div class="body">
                        <form action="{{ route('author.groupclient.store') }}" method="POST" >
                            {{ csrf_field() }}
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="name">
                                    <label class="form-label">Name</label>
                                </div>
                            </div>


                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('author.groupclient.index') }}">BACK</a>
                            <a  class="btn btn-info m-t-15 waves-effect" href="{{ route('author.client.create') }}">ADD CLIENT</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

@endpush