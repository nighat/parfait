@extends('layouts.app')

@section('title','client')
<script>
    function myFunction() {
        window.print();
    }
</script>
@push('css')

<link href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <br>
        <br>
        <div class="row clearfix">
            <div class="col-lg-10 col-md-12 col-sm-push-1">
                <div class="card ">
                    <div class="header bg-cyan ">

                        <h1 style="text-align: center;">
                          Parfait
                        </h1>
                        <p style="text-align: center">
                         Sughandha R/A,Ctg.
                        </p>
                    </div>
                    <div class="body">
                        <table style="width:100%">
                            <tr>
                                <td>{{ $client->name }} </td>
                                <td style="text-align: right">{{ $client->date}}</td>


                            </tr>
                            <tr>
                                <td>{{ $client->address}}</td>


                            </tr>
                            <tr>
                                <td>{{ $client->email}}</td>


                            </tr>
                            <tr>
                                <td>{{ $client->phone}}</td>


                            </tr>
                            <tr>
                                <td>{{ $client->web_address}}</td>


                            </tr>
                        </table>


                       <br/>
                        <div class="table-responsive">
                            <h2 style="text-align: center">
                                Invoice
                            </h2>
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                                <thead>
                                <tr>

                                    <th>Type Of Service </th>
                                    <th>Amount</th>


                                </tr>
                                </thead>
                                <tfoot>
                                <th  style="text-align: right">Grand Total </th>
                                <th>{{ $client->total}}</th>
                                </tfoot>
                                <tbody>




                                    <tr>

                                        <td >@foreach($client->clientservices as $clientservice)
                                                <span >{{ $clientservice->name }}<br/></span>
                                            @endforeach</td>
                                        <td >

                                        </td>

                                    </tr>
                                    <tr>

                                        <td >Previous Payment Info</td>
                                        <td >
                                            {{ $client->pp_date}}
                                        </td>

                                    </tr>
                                    <tr>
                                        <td  style="text-align: right">

                                            Advance <br/><br/>
                                            Due <br/><br/>

                                            Total <br/>


                                        </td>
                                        <td rowspan="2">
                                            {{ $client->advance}}<br/><br/>
                                            {{ $client->total-$client->advance}}<br/><br/>
                                            {{ $client->total}}

                                        </td>
                                    </tr>


                                </tbody>
                            </table>


                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>


                            <table width="100%">
                                <tr>
                                    <td align="left" style="width: 50%;">

                                     <p> ------------------</p>
                                        <p> Client Signature</p>

                                    </td>
                                    <td align="right" style="width: 50%;">
                                        <p>----------------------</p>
                                        <p> Company Signature</p>
                                    </td>
                                </tr>


                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    @endsection

    @push('js')
            <!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
<script src="{{ asset('backend/js/pages/tables/jquery-datatable.js') }}"></script>


<script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>

    @endpush






        {{--<!DOCTYPE html>--}}
{{--<html lang="{{ app()->getLocale() }}">--}}
{{--<head>--}}
    {{--<meta charset="utf-8">--}}
    {{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

    {{--<!-- CSRF Token -->--}}
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

    {{--<title></title>--}}
    {{--<script>--}}
    {{--function myFunction() {--}}
    {{--window.print();--}}
    {{--}--}}
    {{--</script>--}}
    {{--@push('css')--}}

    {{--<link href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">--}}
    {{--@endpush--}}
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
{{--</head>--}}
{{--<body>--}}
{{--<div id="app">--}}
    {{--<nav class="navbar navbar-default navbar-static-top">--}}
        {{--<div class="container">--}}
            {{--<div class="navbar-header">--}}

                {{--<!-- Collapsed Hamburger -->--}}
                {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">--}}
                    {{--<span class="sr-only">Toggle Navigation</span>--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                {{--</button>--}}

                {{--<!-- Branding Image -->--}}
                {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
                {{--{{ config('app.name', 'Laravel') }}--}}
                {{--</a>--}}
                {{--<a class="navbar-brand" href="{{ route('admin.client.index') }}">--}}
                 {{--Client--}}
                {{--</a>--}}
            {{--</div>--}}

            {{--<div class="collapse navbar-collapse" id="app-navbar-collapse">--}}
                {{--<!-- Left Side Of Navbar -->--}}
                {{--<ul class="nav navbar-nav">--}}
                    {{--&nbsp;--}}
                {{--</ul>--}}

                {{--<!-- Right Side Of Navbar -->--}}
                {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<!-- Authentication Links -->--}}
                    {{--@guest--}}
                    {{--<li><a href="{{ route('login') }}">Login</a></li>--}}
                    {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                    {{--@else--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>--}}
                                {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                            {{--</a>--}}

                            {{--<ul class="dropdown-menu">--}}
                                {{--<li>--}}
                                    {{--<a href="{{ route('logout') }}"--}}
                                       {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                                        {{--Logout--}}
                                    {{--</a>--}}

                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                        {{--{{ csrf_field() }}--}}
                                    {{--</form>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--@endguest--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</nav>--}}
{{--<div class="container">--}}
    {{--<div class="container-fluid">--}}
    {{--<!-- Vertical Layout | With Floating Label -->--}}

    {{--<button onclick="myFunction()">Invoice</button>--}}
    {{--<br>--}}
    {{--<br>--}}
    {{--<div class="row clearfix">--}}
    {{--<div class="col-lg-16 col-md-12 col-sm-12 col-xs-12">--}}
    {{--<div class="card ">--}}
    {{--<div class="header bg-cyan ">--}}

    {{--<h1 style="text-align: center">--}}
    {{--Parfait--}}
    {{--</h1>--}}
    {{--<p style="text-align: center">--}}
    {{--Sughandha R/A,Ctg.--}}
    {{--</p>--}}
    {{--</div>--}}
    {{--<div class="body">--}}
    {{--<table style="width:100%">--}}
    {{--<tr>--}}
    {{--<td>{{ $client->name }} </td>--}}
    {{--<td style="text-align: right">{{ $client->date}}</td>--}}


    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>{{ $client->address}}</td>--}}


    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>{{ $client->email}}</td>--}}


    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>{{ $client->phone}}</td>--}}


    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>{{ $client->web_address}}</td>--}}


    {{--</tr>--}}
    {{--</table>--}}


    {{--<br/>--}}
    {{--<div class="table-responsive">--}}
    {{--<h2 style="text-align: center">--}}
    {{--Invoice--}}
    {{--</h2>--}}
    {{--<table class="table table-bordered table-striped table-hover dataTable js-exportable">--}}

    {{--<thead>--}}
    {{--<tr>--}}

    {{--<th>Type Of Service </th>--}}
    {{--<th>Amount</th>--}}

    {{--</tr>--}}
    {{--</thead>--}}
    {{--<tfoot>--}}
    {{--<th colspan="5" style="text-align: right">Subtotal </th>--}}
    {{--<th>{{ $client->total}}</th>--}}
    {{--</tfoot>--}}
    {{--<tbody>--}}


    {{--<tr>--}}

    {{--<td>@foreach($client->clientservices as $clientservice)--}}
    {{--<span >{{ $clientservice->name }}<br/></span>--}}
    {{--@endforeach</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td  style="text-align: right">Advance </td>--}}
    {{--<td rowspan="3">--}}
    {{--{{ $client->advance}}<br/><br/>--}}
    {{--{{ $client->due}}<br/><br/>--}}
    {{--{{ $client->total}}--}}

    {{--</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td style="text-align: right">Due </td>--}}
    {{--<td >{{ $client->due}}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td style="text-align: right">Total </td>--}}
    {{--<td >{{ $client->total}}</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td style="text-align: right">Sub Total </td>--}}
    {{--<td >{{ $client->total}}</td>--}}
    {{--</tr>--}}


    {{--</tbody>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--</div>--}}

    {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<!-- Scripts -->--}}
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
{{--</body>--}}
{{--</html>--}}



