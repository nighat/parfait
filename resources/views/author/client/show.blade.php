@extends('layouts.backend.app')

@section('title','client')

@push('css')

@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <a href="{{ route('author.client.index') }}" class="btn btn-danger waves-effect">
            <i class="material-icons">arrow_back</i>

            <span>Back List </span>
        </a>
        <a href="{{route('author.client.edit',$client->id)}}" class="btn btn-primary waves-effect" >
            <i class="material-icons">edit</i>
            <span>Edit  </span>

        </a>
        <a href="{{route('author.',$client->id)}}" class="btn btn-primary waves-effect" >
            <i class="material-icons">edit</i>
            <span> Invoice </span>

        </a>

        <br>
        <br>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header bg-cyan ">
                        <h2>
                            Client Details


                        </h2>
                    </div>
                    <div class="body">
                        <h5>Project Start Date:-   {{ $client->date}}</h5>
                        <h5>Client Name:-   {{ $client->name }}</h5>
                        <h5>Address:-   {{ $client->address}}</h5>
                        <h5>Phone:-   {{ $client->phone}}</h5>
                        <h5>E-mail:-   {{ $client->email}}</h5>
                        <h5>Website Address:-   {{ $client->web_address}}</h5>



                    </div>
                </div>

                <div class="card ">
                    <div class="header bg-cyan ">
                        <h2>
                            Client Reference


                            </a>

                        </h2>
                    </div>
                    <div class="body">

                        <h5>Reference:-   {{ $client->reference}}</h5>
                        <h5>Reference Value:-   {{ $client->reference_value}}</h5>


                    </div>
                </div>
                <div class="card ">
                    <div class="header bg-cyan ">
                        <h2>
                            Client Progress



                        </h2>
                    </div>
                    <div class="body">


                        <h5>Time Line:-   {{ $client->timeline}}</h5>
                        <h5>Done Service:-   {{ $client->done}}</h5>
                        <h5>Running Service:-   {{ $client->running}}</h5>
                        <h5>Not CompleteService:-   {{ $client->not_complete}}</h5>
                        <h5>Progress:-   {{ $client->progress}}</h5>



                    </div>
                </div>
                <div class="card ">
                    <div class="header bg-cyan ">
                        <h2>
                            Client Payment



                        </h2>
                    </div>
                    <div class="body">

                        <h5>Previous Payment Info:-   {{ $client->pp_date}}</h5>
                        <h5>Advance:-   {{ $client->advance}}</h5>
                        <h5>Due:-   {{ $client->total-$client->advance}}</h5>
                        <h5>Total:-   {{ $client->total}}</h5>

                        <h5>Status:-   {{ $client->status}}</h5>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-cyan">
                        <h2>
                            Client Group
                        </h2>
                    </div>
                    <div class="body">
                        @foreach($client->groupclients as $groupclient)
                            <span class="label bg-orange">{{ $groupclient->name }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="card">
                    <div class="header bg-green">
                        <h2>
                            Type of Service
                        </h2>
                    </div>
                    <div class="body">
                        @foreach($client->clientservices as $clientservice)
                            <span class="label bg-orange">{{ $clientservice->name }}</span>
                        @endforeach
                    </div>
                </div>



            </div>
        </div>
    </div>
    @endsection

    @push('js')

            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->
    <script src="{{ asset('backend/plugins/tinymce/tinymce.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script>
        $(function () {
            //TinyMCE
            tinymce.init({
                selector: "textarea#tinymce",
                theme: "modern",
                height: 300,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true
            });
            tinymce.suffix = ".min";
            tinyMCE.baseURL = '{{ asset('backend/plugins/tinymce') }}';
        });
        function approveclient(id) {
            swal({
                title: 'Are you sure?',
                text: "You went to approve this client ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, approve it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                event.preventDefault();
                document.getElementById('approval-form').submit();
            } else if (
                    // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
            ) {
                swal(
                        'Cancelled',
                        'The client remain pending :)',
                        'info'
                )
            }
        })
        }
    </script>

    @endpush






