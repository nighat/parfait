

@extends('layouts.backend.app')

@section('title','Client')

@push('css')
        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <form action="{{ route('author.client.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ADD  CLIENT INFO
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label"> Project Start Date</label>
                                    <input type="date" id="date" class="form-control" name="date">

                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                <input type="text" id="name" class="form-control" name="name">
                                <label class="form-label">Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="address" class="form-control" name="address">
                                    <label class="form-label"> Address</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="number" id="phone" class="form-control" name="phone">
                                  <label class="form-label"> Phone Number</label>
                                 </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="email" id="email" class="form-control" name="email">
                                  <label class="form-label"> E-mail</label>
                                  </div>
                            </div>

                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="web_address" id="web_address" class="form-control" name="web_address">
                                  <label class="form-label">Website Address</label>
                                  </div>
                            </div>

                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="text" id="reference" class="form-control" name="reference">
                                  <label class="form-label"> Reference</label>
                                 </div>
                            </div>

                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="number" id="reference_value" class="form-control" name="reference_value">
                                  <label class="form-label"> Reference value</label>
                                 </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Group And Service
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('groupclients') ? 'focused error' : '' }}">
                                    <label for="groupclient">Select Group</label>
                                    <select name="groupclients[]" id="groupclient" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($groupclients as $groupclient)
                                            <option value="{{ $groupclient->id }}">{{ $groupclient->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('clientservices') ? 'focused error' : '' }}">
                                    <label for="clientservice">Select Type of Service</label>
                                    <select name="clientservices[]" id="clientservice" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($clientservices as $clientservice)
                                            <option value="{{ $clientservice->id }}">
                                                {{ $clientservice->name }}

                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('author.client.index') }}">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button><br/>
                            <a  class="btn btn-info m-t-15 waves-effect" href="{{ route('author.groupclient.create') }}">ADD GROUP</a>
                            <a  class="btn btn-info m-t-15 waves-effect" href="{{ route('author.clientservice.create') }}">ADD SERVICE</a>

                        </div>


                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Progress
                            </h2>
                        </div>
                        <div class="body">


                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="done" id="done" class="form-control" name="done">
                                    <label class="form-label">Done Service</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="running" id="done" class="form-control" name="running">
                                    <label class="form-label"> Running Service</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="not_complete" id="not_complete" class="form-control" name="not_complete">
                                    <label class="form-label">Not Complete Service</label>
                                </div>
                            </div>


                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="timeline" class="form-control" name="timeline">
                                    <label class="form-label"> Timeline</label>
                                </div>
                            </div>
                            <div class="form-group form-float">

                                <label for="progress">Select Progress</label>
                                <select class="form-control show-tick" name="progress" id="progress" >
                                    <option></option>
                                    <option>Complete</option>
                                    <option>Not Complete</option>
                                    <option>Running</option>

                                </select>

                            </div>

                            </div>

                        </div>
                    </div>
                </div>


            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Payment
                            </h2>
                        </div>
                        <div class="body">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="pp_date" class="form-control" name="pp_date">
                                    <label class="form-label"> Previous Payment Info </label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="total" class="form-control" name="total">
                                    <label class="form-label">Total</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="advance" class="form-control" name="advance">
                                    <label class="form-label"> Advance</label>
                                </div>
                            </div>


                            <div class="form-group form-float">

                                <label for="status">Select Status</label>
                                <select class="form-control show-tick" name="status" id="status" >
                                    <option>Due</option>
                                    <option>Full Paid</option>

                                </select>

                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </form>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->


    @endpush