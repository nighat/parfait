<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
class Client extends Model
{

  //  use SoftDeletes;
    protected $fillable =['name','phone','email','web_address','address','reference','reference_value','progress','advance','due','total','status','groupclient_id','clientservice_id'];


    public function scopeIndex($query){
        return $query->orderBy('id','DESC')->get();
    }

    public function scopeTrash($query)
    {
        return $query->onlyTrashed()->orderBy('id', 'desc')->get();
    }
    public function groupclients()
    {
        return $this->belongsToMany('App\Groupclient')->withTimestamps();
    }

    public function clientservices()
    {
        return $this->belongsToMany('App\Clientservice')->withTimestamps();
    }

    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }


}
