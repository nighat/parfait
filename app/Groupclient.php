<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groupclient extends Model
{
    public function clients()
    {
        return $this->belongsToMany('App\Client')->withTimestamps();
    }
}
