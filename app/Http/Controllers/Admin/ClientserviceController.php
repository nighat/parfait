<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clientservice;
use Brian2694\Toastr\Facades\Toastr;

class ClientserviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientservices= Clientservice::latest()->get();
        return view('admin.clientservice.index',compact('clientservices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clientservice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',



        ]);

        $clientservice = new Clientservice;
        $clientservice->name = $request->name;
        $clientservice->save();
        Toastr::success('Service  Successfully Saved :)','Success');
        return redirect(route('admin.clientservice.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientservice= Clientservice::where('id',$id)->first();
        return view('admin.clientservice.edit',compact('clientservice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',

        ]);

        $clientservice = Clientservice::find($id);
        $clientservice->name = $request->name;
        $clientservice->save();
        Toastr::success('Service Successfully Updated :)','Success');
        return redirect(route('admin.groupclient.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Clientservice::where('id',$id)->delete();
        return redirect()->back();
    }
}
