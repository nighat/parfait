<?php

namespace App\Http\Controllers\Author;

use App\Groupclient;
use App\Clientservice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::latest()->get();
        return view('author.client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $groupclients = Groupclient::all();
        $clientservices = Clientservice::all();
        return view('author.client.create', compact('groupclients','clientservices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $client = new Client;
        $client->date = $request->date;
        $client->name = $request->name;
        $client->address= $request->address;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->web_address = $request->web_address;
        $client->reference = $request->reference;
        $client->reference_value = $request->reference_value;
        $client->done = $request->done;
        $client->running= $request->running;
        $client->not_complete= $request->not_complete;
        $client->timeline= $request->timeline;
        $client->progress= $request->progress;
        $client->pp_date= $request->pp_date;
        $client->advance= $request->advance;
        $client->total= $request->total;
        $client->status= $request->status;
        $client->save();

        $client->groupclients()->attach($request->groupclients);
        $client->clientservices()->attach($request->clientservices);
        Toastr::success('Client Successfully Saved :)','Success');
        return redirect(route('author.client.index'));

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Client $client)
    {
        return view('author.client.show',compact('client'));
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $groupclients = Groupclient::all();
        $clientservices = Clientservice::all();
        $client= Client::where('id',$id)->first();
        return view('author.client.edit',compact('client','groupclients','clientservices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $client->date = $request->date;
        $client->name = $request->name;
        $client->address= $request->address;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->web_address = $request->web_address;
        $client->reference = $request->reference;
        $client->reference_value = $request->reference_value;
        $client->done = $request->done;
        $client->running= $request->running;
        $client->not_complete= $request->not_complete;
        $client->timeline= $request->timeline;
        $client->progress= $request->progress;
        $client->pp_date= $request->pp_date;
        $client->advance= $request->advance;
        $client->total= $request->total;
        $client->status= $request->status;
        $client->save();
        $client->groupclients()->sync($request->groupclients);
        $client->clientservices()->sync($request->clientservices);
        Toastr::success('Client Successfully Updated :)','Success');
        return redirect(route('author.client.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::where('id',$id)->delete();
        return redirect()->back();
    }

    public function clientReport()
    {
        $clients = Client::latest()->get();
        return view('author.client.clientReport', compact('clients'));
    }
//    public function clientInvoice( $id)
//    {
//        $client = Client::find($id);
//        return view('admin.client.clientInvoice',compact('client'));
//
//    }
    public function clientInvoice( $id)
    {
        $client = Client::find($id);
        return view('author.client.clientInvoice',compact('client'));

    }


}
