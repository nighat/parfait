<?php

namespace App\Http\Controllers\Author;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Groupclient;
use Brian2694\Toastr\Facades\Toastr;
class GroupclientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupclients= Groupclient::latest()->get();
        return view('author.groupclient.index',compact('groupclients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('author.groupclient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',



        ]);

        $groupclient = new Groupclient;
        $groupclient->name = $request->name;
        $groupclient->save();
        Toastr::success('Client Group Successfully Saved :)','Success');
        return redirect(route('author.groupclient.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groupclient= Groupclient::where('id',$id)->first();
        return view('author.groupclient.edit',compact('groupclient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',

        ]);

        $groupclient = Groupclient::find($id);
        $groupclient->name = $request->name;
        $groupclient->save();
        Toastr::success('Client Group Successfully Updated :)','Success');
        return redirect(route('author.groupclient.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Groupclient::where('id',$id)->delete();
        return redirect()->back();
    }
}