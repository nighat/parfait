<?php

namespace App\Http\Controllers\Author;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Client;

class ClientController1 extends Controller
{
    public function index()
    {
        $clients = Client::latest()->get();
        return view('author.client.index', compact('clients'));
    }
}
