<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientservice extends Model
{
    public function clients()
    {
        return $this->belongsToMany('App\Client')->withTimestamps();
    }
}
